# Squiz Coding Challenge - Front End

## Requirements

 - The change happens at a breakpoint
 - The content can grow naturally, no fixed heights
 - Tabs shrink to fit text width (not cover the full width)
 - The component can be reused, placed multiple times on the same page


## Solution

This application is implemented using React and Typescript. To run it install the dependencies by executing following command
```
npm install
```
Once the dependencies are installed then execute the following command
```
npm start
```
When the application is running, the UI can be browsed using following url

http://localhost:3000/