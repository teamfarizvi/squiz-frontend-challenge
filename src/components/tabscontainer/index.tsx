import React, {useEffect, useState} from 'react';
import TabModel from '../../app/models/tabmodel';
import Tab from "../tab";
import { tabsData } from "../../app/data/tabsdata";

const TabsContainer = () => {
    const [tabs, setTabs] = useState<TabModel[]>([]);

    const [active, setActive] = useState<string>(tabsData[0].label);

    const tabClicked = (tab: string) => setActive(tab);

    useEffect(() => {
        setTabs(tabsData);
    }, []);

    return (
        <div>
            <ul className='tabs'>
                {tabs && tabs.map((tab: TabModel, i) => (
                    <li
                        key={`label-${i}`}
                        className={active === tab.label? 'active': ''}
                        onClick={() => tabClicked(tab.label)}
                        data-test={tab.label}
                    >
                        {tab.label}
                    </li>
                ))}
            </ul>
            <div className='tab_container'>
                {tabs && tabs.map((tab: TabModel, i) => (
                    <Tab
                        key={`tab-${i}`}
                        label={tab.label}
                        title={tab.title}
                        description={tab.description}
                        activeTab={active}
                        onClick={() => tabClicked}
                    />
                ))}
            </div>
        </div>
    );
}

export default TabsContainer;