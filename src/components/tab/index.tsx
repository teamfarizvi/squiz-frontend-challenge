import React, {useState} from "react";
import TabModel from "../../app/models/tabmodel";
import {tabsData} from "../../app/data/tabsdata";

const Tab = (tab: TabModel) => {
    const [active, setActive] = useState<string>('');
    const [selectedTabData, setSelectedTabData] = useState<TabModel>();
    const tabClicked = (tab: string) => {
        setActive(tab)
        const selectedTab = tabsData.find(t => t.label === tab);
        setSelectedTabData(selectedTab);
        console.log(selectedTabData?.label);
    };

    return (
        <div onClick={() => tabClicked(selectedTabData ? selectedTabData.label : tab.label)}>
            <h3
                className={(selectedTabData ? selectedTabData.label : tab.label) === active
                    ? "d_active tab_drawer_heading"
                    : "tab_drawer_heading"}
            >
                {selectedTabData ? selectedTabData.label : tab.label}
            </h3>
            <div
                id={selectedTabData ? selectedTabData.label : tab.label}
                className={(selectedTabData ? selectedTabData.label : tab.label) === tab.activeTab
                    ? "tab_content"
                    : "tab_content_hidden"}
                data-test='tabContent'
            >
                <h2 data-test="tabTitle">{selectedTabData ? selectedTabData.title : tab.title}</h2>
                <p>{selectedTabData ? selectedTabData.description : tab.description}</p>
            </div>
        </div>
    );
}

export default Tab;