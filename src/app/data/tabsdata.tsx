import TabModel from "../models/tabmodel";

export const tabsData: TabModel[] = [
    {
        label: 'Tab 1',
        title: 'Tab 1 title',
        description: 'Tab 1 content'
    },
    {
        label: 'Tab 2',
        title: 'Tab 2 title',
        description: 'Tab 2 content'
    },
    {
        label: 'Tab 3',
        title: 'Tab 3 title',
        description: 'Tab 3 content'
    },
    {
        label: 'Tab 4',
        title: 'Tab 4 title',
        description: 'A quick brown fox jumped over the lazy dog'
    }
];