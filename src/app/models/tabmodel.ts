export default interface TabModel {
    label: string;
    title: string;
    description: string;
    activeTab?: string;
    onClick?: () => void;
}