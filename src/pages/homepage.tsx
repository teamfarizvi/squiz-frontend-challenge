import React from "react";
import {TabsContainer} from "../components";

const HomePage = () => {
    return (
        <div className="container">
            <TabsContainer />
        </div>
    );
}

export default HomePage;